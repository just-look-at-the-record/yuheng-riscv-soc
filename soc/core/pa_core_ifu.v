`timescale 1ns / 1ps
/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

module pa_core_ifu (
    clk_i,                      // system clock
    rst_n_i,                    // system reset

    data_addr_i,                // d-data address
    data_data_i,                // d-data data
    data_data_o,                // d-data data
    data_we_i,                  // d-data write enable

    inst_addr_i,                // i-data address
    inst_data_o                 // i-data data
    );

`include "../pa_chip_param.v"

input                           clk_i;
input                           rst_n_i;

input  [`ADDR_BUS_WIDTH-1:0]    data_addr_i;
input  [`DATA_BUS_WIDTH-1:0]    data_data_i;
output [`DATA_BUS_WIDTH-1:0]    data_data_o;
input                           data_we_i;

input  [`ADDR_BUS_WIDTH-1:0]    inst_addr_i;
output [`DATA_BUS_WIDTH-1:0]    inst_data_o;

wire                            clk_i;
wire                            rst_n_i;

wire [`ADDR_BUS_WIDTH-1:0]      data_addr_i;
wire [`DATA_BUS_WIDTH-1:0]      data_data_i;
wire [`DATA_BUS_WIDTH-1:0]      data_data_o;
wire                            data_we_i;

wire [`ADDR_BUS_WIDTH-1:0]      inst_addr_i;
wire [`DATA_BUS_WIDTH-1:0]      inst_data_o;


pa_core_itcm u_pa_core_itcm (
    .clk_i                      (clk_i),
    .rst_n_i                    (rst_n_i),

    .data_addr_i                (data_addr_i),
    .data_data_i                (data_data_i),
    .data_data_o                (data_data_o),
    .data_we_i                  (data_we_i),

    .inst_addr_i                (inst_addr_i),
    .inst_data_o                (inst_data_o)
);

endmodule