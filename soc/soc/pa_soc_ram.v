`timescale 1ns / 1ps
/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

module pa_soc_ram (
    clk_i,                      // system clock
    rst_n_i,                    // system reset

    addr_i,                     // address

    data_we_i,                  // write enable
    data_size_i,                // write size
    data_i,                     // write data

    data_o                      // read data
    );

`include "../pa_chip_param.v"

input                           clk_i;
input                           rst_n_i;

input  [`ADDR_BUS_WIDTH-1:0]    addr_i;

input                           data_we_i;
input  [2:0]                    data_size_i;
input  [`DATA_BUS_WIDTH-1:0]    data_i;

output [`DATA_BUS_WIDTH-1:0]    data_o;

wire                            clk_i;
wire                            rst_n_i;

wire [`ADDR_BUS_WIDTH-1:0]      addr_i;

wire                            data_we_i;
wire [2:0]                      data_size_i;
wire [`DATA_BUS_WIDTH-1:0]      data_i;

wire [`DATA_BUS_WIDTH-1:0]      data_o;


wire [`ADDR_BUS_WIDTH-1:0]      addr;

assign addr[`ADDR_BUS_WIDTH-1:0] = {2'b0, addr_i[31:2]};

`ifdef RAM_MODE_BRAM

reg  [3:0]                      addr_mask;

always @ (*) begin
case (addr_i[1:0])
    2'b00 : addr_mask[3:0] <= 4'b1111;
    2'b01 : addr_mask[3:0] <= 4'b1110;
    2'b10 : addr_mask[3:0] <= 4'b1100;
    2'b11 : addr_mask[3:0] <= 4'b1000;
endcase
end

wire [`DATA_BUS_WIDTH-1:0]      _data;

blk_mem_ram u_blk_mem_ram0 (
    .clka                       (clk_i),
    .wea                        (data_we_i & addr_mask[3]),
    .addra                      (addr),
    .dina                       (data_i[31:24]),
    .douta                      (_data[31:24])
);

blk_mem_ram u_blk_mem_ram1 (
    .clka                       (clk_i),
    .wea                        (data_we_i & addr_mask[2]),
    .addra                      (addr),
    .dina                       (data_i[23:16]),
    .douta                      (_data[23:16])
);

blk_mem_ram u_blk_mem_ram2 (
    .clka                       (clk_i),
    .wea                        (data_we_i & addr_mask[1]),
    .addra                      (addr),
    .dina                       (data_i[15:8]),
    .douta                      (_data[15:8])
);

blk_mem_ram u_blk_mem_ram3 (
    .clka                       (clk_i),
    .wea                        (data_we_i & addr_mask[0]),
    .addra                      (addr),
    .dina                       (data_i[7:0]),
    .douta                      (_data[7:0])
);

`else

reg  [`DATA_BUS_WIDTH-1:0]      _ram[0:`RAM_SIZE*1024-1];

initial begin
    for (integer i=0; i<`RAM_SIZE*1024; i=i+1) begin
        _ram[i] = `ZERO_WORD;
    end
end

always @ (posedge clk_i) begin
    if (data_we_i) begin
    case (addr_i[1:0])
        2'b00 : _ram[addr] <= {data_i[31:0]                   };
        2'b01 : _ram[addr] <= {data_i[31:8],  _ram[addr][7:0] };
        2'b10 : _ram[addr] <= {data_i[31:16], _ram[addr][15:0]};
        2'b11 : _ram[addr] <= {data_i[31:24], _ram[addr][23:0]};
    endcase
    end
end

reg  [`DATA_BUS_WIDTH-1:0]      _data;

always @ (posedge clk_i) begin
    if (~rst_n_i) begin
        _data[`DATA_BUS_WIDTH-1:0] = `ZERO_WORD;
    end
    else begin
        _data[`DATA_BUS_WIDTH-1:0] = _ram[addr];
    end
end

`endif //`ifdef RAM_MODE_BRAM

assign data_o[`DATA_BUS_WIDTH-1:0] = _data[`DATA_BUS_WIDTH-1:0];

endmodule