set proj_dir [file dirname [info script]]

cd $proj_dir

set prj_name "yuheng-riscv-soc"
set prj_part_name "xc7a35tfgg484-1"

set prj_path "$proj_dir/project"
set prj_source "$proj_dir"

create_project $prj_name $prj_path -part $prj_part_name -force

add_files [glob $prj_source/*.v]
add_files [glob $prj_source/core/*.v]
add_files [glob $prj_source/soc/*.v]
add_files [glob $prj_source/perips/*.v]

set files [list \
 "[file normalize "$prj_source/ip/blk_mem_itcm/blk_mem_itcm.xci"]"\
]
set imported_files [import_files -fileset sources_1 $files]

set files [list \
 "[file normalize "$prj_source/ip/blk_mem_ram/blk_mem_ram.xci"]"\
]
set imported_files [import_files -fileset sources_1 $files]

set_property SOURCE_SET sources_1 [get_filesets sim_1]

add_files -fileset sim_1 -norecurse "$prj_source/tb/core_tb.v"
add_files -fileset sim_1 -norecurse "$prj_source/tb/core_uart_monitor_tb.v"

set_property simulator_language Verilog [current_project]

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1