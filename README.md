﻿9:49 2021/12/3

## 〇、前言

> 在古代中国，人们将北斗七星中最亮的那颗星称为玉衡。亦称：廉贞星。  

## 一、简介

玉衡（YuHeng）基于开源 RISC-V 指令集架构，是一款极简的 32 位 RISC-V 处理器核。仅用于检测和实验。  

**玉衡不具备任何商用价值，但可以作为很好的学习平台。**  

使用玉衡，能够很好的了解计算机体系结构、操作系统、通信协议、编译原理等相关知识。  
另外，所有的运行环境均为精简搭建，因此还可以很轻易的熟悉搭建过程。  

不仅会用，还要知道为什么会用。  

> “这么说吧，就是为了这口醋，我才包的这顿饺子。”  

### 特点与功能

1. 支持 RV32IMFD 扩展指令集，通过RISC-V指令兼容性测试  

2. 采用五级流水线设计：IF、ID、EX、MEM、WB  

3. 采用自定义的总线设计：RBM（RISC-V Bus Matrix）  

4. 支持中断：矢量中断和非矢量中断  

5. 支持多种外设： UART、TIMER 等  

6. 支持 C 程序运行  

7. 支持 RT-Thread Nano 3.1.5  

8. 支持 UART 模拟器，可在仿真环境中进行调试  

9. ...  

## 二、功能介绍

### 1. 整体框图

![](https://gitee.com/backheart/picgo-image/raw/master/img/Yu-Heng.jpg)  

**框图中部分模块暂时未支持。**  

### 2. 地址分配

为了简化取指阶段的操作，使用 ITCM 来存放程序代码。因此程序空间与数据空间是相互独立的。  

**ROM、RAM 被认为是外设**，所有的外设统一编址在 4G 的空间上。  

|起始地址   |结束地址   |大小|外设 |支持|
|:-:        |:-:        |:-: |:-:  |:-: |
|0x0000_0000|0x0fff_ffff|256M|ROM  |√   |
|0x1000_0000|0x1fff_ffff|256M|RAM  |√   |
|0x2000_0000|0x2fff_ffff|256M|TIMER|√   |
|0x3000_0000|0x3fff_ffff|256M|UART |√   |
|0x4000_0000|0x4fff_ffff|256M|I2C  |×   |
|0x5000_0000|0x5fff_ffff|256M|SPI  |×   |
|0x6000_0000|0x6fff_ffff|256M|LCD  |×   |
|...        |...        |... |...  |    |

其中 ROM 和 ITCM 是同一块区域，二者的地址是相同的。  

**目前挂载了 3 个 UART 模块，用于测试 UART 收发功能。**  

## 三、使用说明

硬件部分的设计源码存放在 `sources/` 路径下  

- `sources/create_project.tcl`： Vivado 工程创建脚本（由 `auto.bat` 调用）  

- `sources/pa_chip_param.v`：参数文件  

- `sources/pa_chip_top.v`：顶层设计文件  

- `sources/auto.bat`：工程创建脚本  

- `sources/clear.bat`：工程清除脚本  

- `sources/core/`： 存放内核的设计源码  

- `sources/perips/`： 存放外设的设计源码  

- `sources/soc`： 存放存储器和总线控制器的设计源码  

- `sources/tb`： 存放 testbench 源码  

通过直接双击 `auto.bat` 即可实现 Vivado 工程的创建。  

## 四、致谢

在玉衡（YuHeng）的设计过程中，参考和借鉴了许多优秀的开源项目。  

1. tinyriscv 项目  

> https://gitee.com/liangkangnan/tinyriscv.git  

2. e200_opensource 项目  

> https://github.com/SI-RISCV/e200_opensource.git  

## 五、修改日志

- v1.1  
  - 添加 C 语言测试用例（12:25 2021/12/21）  

- v1.0  
  - 支持 RT-Thread Nano 3.1.5（10:25 2021/12/03）  

## 六、维护

如果有任何疑问或者建议，欢迎在下方评论，或者通过邮件联系（E-mail：ytesliang@163.com），我会尽可能在 24 小时内进行回复。  

ATONEMAN  
2021.12.03  
